package modelo;

import java.util.ArrayList;

public class Agencia {

	private int numero;
	private String endereco;
	
	private static int contador;
	public ArrayList<Conta> contas;
	
	public Agencia() {
		new Agencia("N�o informado");
	}
	
	public Agencia(String endereco) {
		contador++;
		this.numero = contador;
		this.endereco = endereco;
		contas = new ArrayList<Conta>();
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public String getEndereco() {
		return endereco;
	}
	
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	
	public int numeroDeContas() {
		return contas.size();
	}
	
	public double saldoTotal() {
		double total = 0;
		
		for (Conta conta : contas)
			total = total + conta.getSaldo();
		
		return total;
	}
	
	public String toString() {
		String retorno = "Agencia:\n   N�mero: " + numero + "\n   Endere�o: " + endereco + "\n   Qtde. de Contas: " + numeroDeContas() + "\n   Saldo Total da Ag�ncia: R$ " + saldoTotal() ;
		retorno += "\n\nContas:\n ";
		
		for (Conta conta : contas)
		    retorno += " - " + conta + "\n"; 
		
		return retorno;
	}	
}
