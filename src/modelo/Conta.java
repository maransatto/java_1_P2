package modelo;

public class Conta {

	protected int numero;
	protected double saldo;
	
	private Cliente cliente; 
	
	private static int codigo;
	
	public Conta(Cliente cliente) {
		new Conta(cliente, 0.00);
	}
	public Conta(Cliente cliente, double valorInicial) {
		codigo++;
		this.numero = codigo;
		this.cliente = cliente;
		this.saldo = valorInicial;
	}
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public String toString() {
		return cliente + "\nConta: \n   N�mero: " + numero + "\n   Saldo: " + saldo;
	}
	
}
