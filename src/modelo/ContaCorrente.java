package modelo;

public class ContaCorrente extends Conta {

	private double limite;
	
	public ContaCorrente(Cliente cliente) {
		super(cliente);
		new ContaCorrente(cliente, 0.00);
	}
	public ContaCorrente(Cliente cliente, double valorInicial) {
		super(cliente, valorInicial);
		new ContaCorrente(cliente, valorInicial, 0.00);
	}
	public ContaCorrente(Cliente cliente, double valorInicial, double limiteInicial) {
		super(cliente, valorInicial);
		this.limite = limiteInicial;
	}

	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}
	
	public boolean sacar(double valor) {
		
		if (valor <= (saldo + limite)) {
			saldo = saldo - valor;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean depositar(double valor) {
		
		if (valor < 0) {
			return false;
		} else {
			saldo = saldo + valor;
			return true;
		}
	}
	
	public String toString() {
		return "---- Corrente -----\n " + super.toString() + "\n   Limite de Cr�dito: R$" + limite;
	}
}
