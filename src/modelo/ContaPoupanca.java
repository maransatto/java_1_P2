package modelo;

public class ContaPoupanca extends Conta {
	
	private double rendimento;
	
	public ContaPoupanca(Cliente cliente) {
		super(cliente);
		new ContaPoupanca(cliente, 0.00);
	}
	public ContaPoupanca(Cliente cliente, double valorInicial) {
		super(cliente, valorInicial);
		new ContaPoupanca(cliente, valorInicial, 2.00);
	}
	public ContaPoupanca(Cliente cliente, double valorInicial, double rendimento) {
		super(cliente, valorInicial);
		this.rendimento = rendimento;
	}

	public double getRendimento() {
		return rendimento;
	}

	public void setRendimento(double rendimento) {
		if (rendimento > 0) {
			this.rendimento = rendimento;	
		}
	}
	
	public void aplicarRendimento() {
		saldo = saldo + saldo * (rendimento/100);
	}

	public String toString() {
		return "---- Poupan�a-----\n " + super.toString() + "\n   Rendimento: " + rendimento + "%";
	}
	
}
