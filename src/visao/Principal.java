package visao;

import javax.swing.JOptionPane;

import modelo.*;

public class Principal {

	public static void main(String[] args) {
		
		// managing clients
		
		Cliente tsen = new Cliente("Christopher Martinez Tsen", "123.456.789-10");
		Cliente maransatto = new Cliente("Fernando Silva Maransatto", "35429573809");
		
		// managing accounts
		
		ContaPoupanca conta_tsen = new ContaPoupanca(tsen, 5000.00, 5.00);
		ContaCorrente conta_maransatto = new ContaCorrente(maransatto, 1000.00, 4000);
		
		// Aplicar Rendimento da Conta Poupan�a
		String detalhes_conta_tsen = "Informa��es Iniciais da Conta:\n" + conta_tsen + "\n\n";
		conta_tsen.aplicarRendimento();
		detalhes_conta_tsen += "Ap�s Aplicar Rendimento:\n" + conta_tsen;
		
		JOptionPane.showMessageDialog(null, detalhes_conta_tsen);
		
		// Depositar na Conta Corrente
		String deposito_maransatto = "Informa��es Iniciais da Conta:\n" + conta_maransatto + "\n\n";

		String inputValorDeposito = JOptionPane.showInputDialog(null, "Informe o Valor para Dep�sito do Maransatto: ");
		double valor_deposito = Double.parseDouble(inputValorDeposito);

		if (conta_maransatto.depositar(valor_deposito)) {
			deposito_maransatto += "Ap�s Depositar R$ " + valor_deposito + ":\n" + conta_maransatto;	
		} else {
			deposito_maransatto += "Erro: N�o � poss�vel depositar um valor negativo\n" + conta_maransatto;
		}
		
		JOptionPane.showMessageDialog(null, deposito_maransatto);
		
		// Sacar da Conta Corrente
		String saque_corrente = "Informa��es Atuais da Conta:\n" + conta_maransatto + "\n\n";

		String inputSaque = JOptionPane.showInputDialog(null, "Informe o Valor para Saque do Maransatto: ");
		double valor_saque = Double.parseDouble(inputSaque);

		if (conta_maransatto.sacar(valor_saque)) {
			saque_corrente += "Ap�s Sacar R$ " + valor_saque + ":\n" + conta_maransatto;	
		} else {
			saque_corrente += "Erro: Limite de Cr�dito Atingido\n" + conta_maransatto;
		}
		
		JOptionPane.showMessageDialog(null, saque_corrente);
		
		Agencia agencia = new Agencia("Rua Shunji Nishimura, 1000, Pomp�ia - SP, CEP 17580-000");
		agencia.contas.add(conta_tsen);
		agencia.contas.add(conta_maransatto);
		
		JOptionPane.showMessageDialog(null, agencia);
		
	}

}
